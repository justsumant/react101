const express = require('express');
const keys = require('./config/keys');
require('./models/User');
require('./services/passport');
const app = express();
const passport = require('passport');

const cookieSession = require('cookie-session');
const mongoose = require('mongoose');


passport.use(new GoogleStrategy({
    clientID: keys.googleClientID,
    clientSecret: keys.googleClientSecret,
    callbackURL: "/auth/google/callback"
}, (accessToken, refreshToken, profile, done) => {
    
    console.log('profile: ', profile);
    console.log('accesstoken: ', accessToken);
    console.log('refresh token: ', refreshToken);
    

}
));

app.get('/auth/google', passport.authenticate('google', {
    scope: ['profile', 'email']
}));

app.get('/auth/google/callback', passport.authenticate('google'));

app.use(
    cookieSession({
        maxAge: 30 * 24 * 60 * 60 * 1000,
        keys: [keys.cookieKey]
    })
);

app.use(passport.initialize());
app.use(passport.session());
require('./routes/authRoutes')(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT);