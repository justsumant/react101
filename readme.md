#1 npm init and install express

#2 server.js file
const express = require('express');
const app = express();
app.get('/hello', (req, res) => {
    res.send({ hi: 'there from the new application.... ' });
});

const PORT = process.env.PORT || 5000;
app.listen(PORT);

#3 deploy
git init
git add .
git commit -am "message"
heroku login
heroku git:remote -a learn-react-2018
git push heroku master

#4 re deploy
git commit -am "message"
git push heroku master

#5 install passport
npm install --save passport passport-google-oauth20 // 20 means 2.0 version of passport js which is most recommended version to use.


#6 create a google web api creadential
url: localhost:5000
callback/redirect url: http://localhost:5000/auth/google/callBack
then get the clientID and secredKey

#7 code in server.js for passport
// the keys are stored in config/keys.js file as module.export....
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth20').Strategy;
const app = express();
const keys = require('./config/keys');

passport.use(new GoogleStrategy({
    clientID: keys.googleClientID,
    clientSecret: keys.googleClientSecret,
    callbackURL: '/auth/google/callBack'
}, (accessToken, refreshToken, profile, done) => {
    console.log('access token', accessToken);
    console.log('refresh token:', refreshToken);
    console.log('profile: ', profile);
    })
);

app.get('/auth/google', passport.authenticate('google', {
    scope: ['profile', 'email']
}));

app.get('/auth/google/callback', passport.authenticate('google'));

#8 to automatically re-run the server
install nodemon 
add into package.json/scripts
    "dev": "nodemon server.js"
and run using 
    npm run dev

#9

#  ------------------------------------------------------------------------------------------------
const express = require('express');
const keys = require('./config/keys');
require('./models/User');
require('./services/passport');
const app = express();
require('./routes/authRoutes')(app);



const mongoose = require('mongoose');

// mongoose.connect(keys.mongoURI);


const PORT = process.env.PORT || 5000;
app.listen(PORT);



# from passport
// app.use( passport.initialize());
// app.use( passport.session());


// passport.serializeUser((user, done) => {
//     done(null, user.id);
// });
// passport.deserializeUser((id, done) => {
//     User.findById(id).then(user => {
//         done(null, user);
//     });
// });



passport.use(new GoogleStrategy({
    clientID: keys.googleClientID,
    clientSecret: keys.googleClientSecret,
    callbackURL: "/auth/google/callBack"
},
    function (token, tokenSecret, profile, done) {
        User.findOne({ googleID: profile.id }).then((existingUser) => {
            if (existingUser) {
                console.log('existing');
                done(null, existingUser);
            } else {
                console.log('new');
                new User({ googleID: profile.id }).save()
                    .then((user) => {
                        done(null, user);
                    });
            }
        })
        console.log('profile: ', profile);
        console.log('token', token);
        console.log('TOKEN secret', tokenSecret);
    }
));



